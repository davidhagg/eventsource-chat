﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourceChat
{
    using System.IO;
    using System.Net;
    using System.ServiceModel.Syndication;
    using System.Threading;
    using System.Web.Script.Serialization;
    using System.Xml;

    class Program
    {
        private static object syncLock = new object();

        private static string previousPage;

        private static string currentPage;

        private static List<SyndicationItem> allItems = new List<SyndicationItem>();

        private static bool isSynchronized;

        private static void Main(string[] args)
        {
            var streamUri = "http://127.0.0.1:2113/streams/david";

            currentPage = null;
            previousPage = streamUri;

            SyncItems(streamUri);

            ListenForUpdates(currentPage);
            
            Console.Write(">");
            var input = Console.ReadLine();
            while (input.ToLower() != "q")
            {
                var json = CreateJson(input);
                AddMessage(streamUri, json);
                Console.Write(">");
                input = Console.ReadLine();
            }

            Console.ReadLine();
        }

        private static void ListenForUpdates(string uri)
        {
            Task.Factory.StartNew(
                () =>
                    {
                        previousPage = uri;
                        while (true)
                        {
                            lock (syncLock)
                            {
                                currentPage = previousPage;
                                previousPage = ReadNew(currentPage, allItems);
                            }

                            Thread.Sleep(10);
                        }
                    },
                TaskCreationOptions.LongRunning);
        }

        private static void SyncItems(string uri)
        {
            Task.Factory.StartNew(() => GetLastFeed(uri), TaskCreationOptions.LongRunning).Wait();
        }

        private static void AddMessage(string uri, string json)
        {
            using (var wc = new WebClient())
            {
                wc.Headers["Content-Type"] = "application/json";
                wc.UploadString(uri, "POST", json);
            }
        }

        private static void GetLastFeed(string uri)
        {
            // Get AtomPub entry
            string result;
            using (var wc = new WebClient())
            {
                wc.Headers["Accept"] = "application/atom+xml";
                result = wc.DownloadString(uri);
            }

            var reader = XmlReader.Create(new StringReader(result));
            var feed = SyndicationFeed.Load(reader);

            // Get last page and move forward
            var lastPage = feed.Links.FirstOrDefault(x => x.RelationshipType == "last");
            previousPage = lastPage.Uri.ToString();
            
            reader.Close();

            while (!isSynchronized)
            {
                lock (syncLock)
                {
                    currentPage = previousPage;
                    previousPage = ReadFrom(currentPage, allItems);
                }
            }

            PrintItems();
        }

        private static string ReadFrom(string uri, List<SyndicationItem> items)
        {
            string result;
            using (var wc = new WebClient())
            {
                wc.Headers["Accept"] = "application/atom+xml";
                result = wc.DownloadString(uri);
            }

            var reader = XmlReader.Create(new StringReader(result));
            var feed = SyndicationFeed.Load(reader);

            if (feed != null)
            {
                if (!feed.Items.Any())
                {
                    isSynchronized = true;
                }
                else
                {
                    items.AddRange(feed.Items);
                }
            }

            reader.Close();

            var previousLink = feed.Links.FirstOrDefault(x => x.RelationshipType == "previous");
            return previousLink != null ? previousLink.Uri.ToString() : null;
        }

        private static string ReadNew(string uri, List<SyndicationItem> items)
        {
            string result;
            using (var wc = new WebClient())
            {
                wc.Headers["Accept"] = "application/atom+xml";
                result = wc.DownloadString(uri);
            }

            var reader = XmlReader.Create(new StringReader(result));
            var feed = SyndicationFeed.Load(reader);

            if (feed != null)
            {
                if (feed.Items.Any())
                {
                    items.AddRange(feed.Items);
                    foreach (var item in feed.Items)
                    {
                        var jsonLink = item.Links.First(x => x.MediaType == "application/json");
                        PrintMessage(jsonLink.Uri.ToString());
                    }
                }
            }

            reader.Close();

            var previousLink = feed.Links.FirstOrDefault(x => x.RelationshipType == "previous");
            return previousLink != null ? previousLink.Uri.ToString() : null;
        }

        private static void PrintMessage(string uri)
        {
            string result;
            using (var wc = new WebClient())
            {
                wc.Headers["Accept"] = "application/json";
                result = wc.DownloadString(uri);
            }

            var jss = new JavaScriptSerializer();
            var eventMessage = jss.Deserialize<Dictionary<string, object>>(result);

            var chatMessage = eventMessage["data"] as Dictionary<string, object>;

            if (chatMessage != null)
            {
                Console.WriteLine(chatMessage["Message"]);
            }
        }

        private static void PrintItems()
        {
            lock (syncLock)
            {
                foreach (var item in allItems)
                {
                    var jsonLink = item.Links.First(x => x.MediaType == "application/json");
                    PrintMessage(jsonLink.Uri.ToString());
                }
            }
        }

        private static string CreateJson(string data)
        {
            var sb = new StringBuilder();
            sb.Append("{");
            sb.Append("\"CorrelationId\" : \"" + Guid.NewGuid().ToString() + "\",");
            sb.Append("\"ExpectedVersion\" : \"-2\",");
            sb.Append("\"Events\" : [");
            sb.Append("{");
                sb.Append("\"EventId\" : \"" + Guid.NewGuid() + "\",");
                sb.Append("\"EventType\" : \"ChatMsg\",");
                sb.Append("\"Data\" : { \"Message\": \""+ data+ "\"},");
                sb.Append("\"Metadata\" : { \"Something\" : \"AValue\"}");
            sb.Append("}]");
            sb.Append("}");

            return sb.ToString();
        }
    }
}
